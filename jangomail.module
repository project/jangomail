<?php

/**
 * @file
 * Enables Drupal to send e-mail directly using JangoMail
 *
 * This module uses the PHPMailer class, originally by Brent R. Matzelle, now
 * maintained by Codeworx Tech.
 *
 * Overriding mail handling in Drupal requires setting the smtp_library
 * variable with the filename of a file containing a drupal_mail_wrapper()
 * function. This module sets the smtp_library value to point back to this
 * file which contains the drupal_mail_wrapper() function that uses the
 * PHPMailer class to send e-mail instead of the PHP mail() function.
 *
 * @link http://www.jangomail.com/
 */

/**
 * Implementation of hook_help().
 */
function jangomail_help($path, $arg) {
  switch ($path) {
    case 'admin/help#jangomail':
      return t('Allows the sending of site e-mail through an JangoMail');
  }
}  //  End of jangomail_help().



/**
 * Implementation of hook_menu().
 */
function jangomail_menu() {
  $items['admin/settings/jangomail'] = array(
    'title'            => 'JangoMail Support',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('jangomail_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'description'      => 'Allows the sending of site e-mail through JangoMail',
  );

  return $items;
}  //  End of jangomail_menu().



/**
 * Administrative settings.
 *
 * @return
 *   An array containing form items to place on the module settings page.
 */
function jangomail_admin_settings() {
  // Override the smtp_library variable.
  if (variable_get('jangomail_on', 0)) {
    $jangomail_path = drupal_get_filename('module', 'jangomail');
    if ($jangomail_path) {
      variable_set('smtp_library', $jangomail_path);
      drupal_set_message(t('jangomail.module is active.'));
    }
    // If drupal can't find the path to the module, display an error.
    else {
      drupal_set_message(t("jangomail.module error: Can't find file."), 'error');
    }
  }
  // If this module is turned off, delete the variable.
  else {
    variable_del('smtp_library');
    drupal_set_message(t('jangomail.module is INACTIVE.'));
  }

  $form['onoff'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Install options'),
  );
  $form['onoff']['jangomail_on'] = array(
    '#type'          => 'radios',
    '#title'         => t('Turn this module on or off'),
    '#default_value' => variable_get('jangomail_on', 0),
    '#options'       => array(1 => t('On'), 0 => t('Off')),
    '#description'   => t('To uninstall this module you must turn it off here first.'),
  );

  $form['auth'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('jangomail Authentication'),
    '#description' => t('jangomail authentication details'),
  );
  $form['auth']['jangomail_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Jangomail URL'),
    '#default_value' => variable_get('jangomail_url', ''),
    '#description'   => t('jangomail server url. http://69.64.179.112/api.asmx/SendTransactionalEmail'),
  );
  $form['auth']['jangomail_username'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Username'),
    '#default_value' => variable_get('jangomail_username', ''),
    '#description'   => t('jangomail Username.'),
  );
  $form['auth']['jangomail_password'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Password'),
    '#default_value' => variable_get('jangomail_password', ''),
    '#description'   => t('jangomail password.'),
  );

  $form['email_options'] = array(
    '#type'  => 'fieldset',
    '#title' => t('E-mail options'),
  );
  $form['email_options']['jangomail_from'] = array(
    '#type'          => 'textfield',
    '#title'         => t('E-mail from address'),
    '#default_value' => variable_get('jangomail_from', ''),
    '#description'   => t('The e-mail address that all e-mails will be from.'),
  );
  $form['email_options']['jangomail_fromname'] = array(
    '#type'          => 'textfield',
    '#title'         => t('E-mail from name'),
    '#default_value' => variable_get('jangomail_fromname', ''),
    '#description'   => t('The name that all e-mails will be from. If left blank will use the site name of: ') . variable_get('site_name', 'Drupal powered site'),
  );

  // If an address was given, send a test e-mail message.
  $test_address = variable_get('jangomail_test_address', '');
  if ($test_address != '') {
    // Clear the variable so only one message is sent.
    variable_del('jangomail_test_address');
    global $language;
    $params['subject'] = t('Drupal test e-mail');
    $params['body']    = t('If you receive this message it means your site is capable of sending e-mail.');
    drupal_mail('jangomail', 'jangomail-test', $test_address, $language, $params);
    drupal_set_message(t('A test e-mail has been sent to @email. You may want to !check for any error messages.', array('@email' => $test_address, '!check' => l(t('check the logs'), 'admin/reports/dblog'))));
  }
  $form['email_test'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Send test e-mail'),
  );
  $form['email_test']['jangomail_test_address'] = array(
    '#type'          => 'textfield',
    '#title'         => t('E-mail address to send a test e-mail to'),
    '#default_value' => '',
    '#description'   => t('Type in an address to have a test e-mail sent there.'),
  );
  return system_settings_form($form);
}  //  End of jangomail_admin_settings().



/**
 * Validataion for the administrative settings form.
 *
 * @param form
 *   An associative array containing the structure of the form.
 * @param form_state
 *   A keyed array containing the current state of the form.
 */
function jangomail_admin_settings_validate($form, &$form_state) {
  
  if ($form_state['values']['jangomail_on'] == 1 && $form_state['values']['jangomail_url'] == '') {
    form_set_error('jangomail_url', t('You must enter a jangomail url.'));
  }

  if ($form_state['values']['jangomail_on'] == 1 && $form_state['values']['jangomail_username'] == '') {
    form_set_error('jangomail_username', t('You must enter a jangomail username.'));
  }

  if ($form_state['values']['jangomail_on'] == 1 && $form_state['values']['jangomail_password'] == '') {
    form_set_error('jangomail_password', t('You must enter a jangomail password.'));
  }

  if ($form_state['values']['jangomail_from'] && !valid_email_address($form_state['values']['jangomail_from'])) {
    form_set_error('jangomail_from', t('The provided from e-mail address is not valid.'));
  }
}  //  End of jangomail_admin_settings_validate().



/**
 * Sends out the e-mail.
 *
 * @param message
 *   An array with at least the following elements: id, to, subject, body and
 *  headers.
 *
 * @see drupal_mail_send()
 */
function drupal_mail_wrapper($message) {  
  $from     = $message['from'];
  $jango_username = variable_get('jangomail_username', '');
  $jango_password = variable_get('jangomail_password', '');
  $jango_server_url = variable_get('jangomail_url', '');


  // Set the from name and e-mail address.
  if (variable_get('jangomail_fromname', '') != '') {
    $from_name = variable_get('jangomail_fromname', '');
  }
  else {
    // If value is not defined in settings, use site_name.
    $from_name = variable_get('site_name', '');
  }

  // Blank value will let the e-mail address appear.
  if (empty($from)) {
    // If from e-mail address is blank, use jangomail_from config option.
    if ($from = variable_get('jangomail_from', '') == '') {
      // If jangomail_from config option is blank, use site_email.
      if ($from = variable_get('site_email', '') == '') {
        drupal_set_message(t('There is no submitted from address.'), 'error');
        watchdog('jangomail', 'There is no submitted from address.', array(), WATCHDOG_ERROR);
        return FALSE;
      }
    }
  }
  if (preg_match('/^".*"\s*<.*>$/', $from)) {
    // . == Matches any single character except line break characters \r and \n.
    // * == Repeats the previous item zero or more times.
    $from_name = preg_replace('/"(.*)"(.*)/i',   '$1', $from); // It gives: Name
    $from      = preg_replace("/(.*)\<(.*)\>/i", '$2', $from); // It gives: name@domain.tld
  }
  elseif (!valid_email_address($from)) {
    drupal_set_message(t('The submitted from address (@from) is not valid.', array('@from' => $from)), 'error');
    watchdog('jangomail', 'The submitted from address (@from) is not valid.', array('@from' => $from), WATCHDOG_ERROR);
    return FALSE;
  }
  
  $params = 'Username='.urlencode($jango_username).'&Password='.urlencode($jango_password).'&FromEmail='.urlencode($from).
'&FromName='.urlencode($from_name).'&ToEmailAddress='.urlencode($message['to']).'&Subject='.urlencode($message['subject']).
'&MessageHTML='.urlencode($message['body']).'&Options=Wrapping=0&MessagePlain='.urlencode($message['body']);

  
  $crl = curl_init();
  curl_setopt($crl, CURLOPT_URL, $jango_server_url);
  curl_setopt($crl, CURLOPT_POSTFIELDS, $params);
  curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($crl, CURLOPT_POST, 1);
  $result=curl_exec($crl);
  curl_close($crl);

  if (strstr($result, "SUCCESS")){
	return TRUE;
  }
  else
  {
	return FALSE;
  }

}  //  End of drupal_mail_wrapper().


/**
 * Implementation of hook_mail().
 */
function jangomail_mail($key, &$message, $params) {
  if ($key == 'jangomail-test') {
    $message['subject'] = $params['subject'];
    $message['body']    = $params['body'];
  }
}  //  End of jangomail_mail().
